"""Test modul for RomanNumeral class"""

from roman_numeral import RomanNumeral
import unittest


class TestRomanNumeral(unittest.TestCase):
    """Test class for class RomanNumeral"""
   
    def test_number_to_roman(self):
        self.assertEqual("II", RomanNumeral().number_to_roman(2))
    
    def test_number_to_roman_bigger_number(self):
        self.assertEqual("C", RomanNumeral().number_to_roman(100))

    def test_number_to_roman_negative_number(self):
        self.assertEqual("There is no zero nor negative numbers in Roman numerals", RomanNumeral().define_digit_number(-1))

    def test_number_to_roman_zero_number(self):
        self.assertEqual("There is no zero nor negative numbers in Roman numerals", RomanNumeral().define_digit_number(0))

    def test_define_one_digit_number(self):
        self.assertEqual("I", RomanNumeral().define_digit_number(1))

    def test_define_two_digit_number(self):
        self.assertEqual("XXII", RomanNumeral().define_digit_number(22))

    def test_define_three_digit_number(self):
        self.assertEqual("CXXIII",RomanNumeral().define_digit_number(123))

unittest.main()
