# Feature: Roman Numeral
**As a**: customer 
**I want**: to get Roman numeral of any number 
**So that**: any number can be presented as Roman numeral 

## Scenario_1: Defining numbers

**Given**: A dictionary of numbers and their roman values 
**When**:  The searched value is matched with value in that dictionary 
**Then**: Return a Roman number of that given number

## Scenario_2: Giving a one digit number

**Given**: A non Roman number 
**When**:  A non Roman number is one digit 
**Then**: Return a Roman number of that given number

## Scenario_3: Giving a two digit number

**Given**: A non Roman number 
**When**:  A non Roman number is two digit 
**Then**: Return a Roman number of that given number 
 
## Scenario_4: Giving a three digit number

**Given**: A non Roman number 
**When**:  A non Roman number is three digit 
**Then**: Return a Roman number of that given number