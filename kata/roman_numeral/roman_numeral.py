"""Roman Numeral modul"""


class RomanNumeral:
    """RomanNumeral class"""

    def number_to_roman(self, passed_number):
        """
        Function that converts arabic numbers to roman.
        
        Arguments: 
        passed_number-number passed for conversion

        Return: 
        Converted number
        """
        str_value = ""
        dct_numbers = {1: "I",
                       2: "II",
                       3: "III",
                       10: "X",
                       20: "XX",
                       100: "C",
                       }
        for dct_key_val in (dct_numbers.keys()):
            if dct_key_val == passed_number:
                return dct_numbers[dct_key_val]

        
    def define_digit_number(self, passed_number):
        """
        Function that, based on number of digits (one, two or three), parts the number and converts numbers one by one.

        Arguments:
        passed_number-number being passed for parting and converting

        Returns:
        Fully converted number
        """
        final_roman_nmb = ""
        list_of_digits = []
        
        if passed_number<0 or passed_number==0:
                final_roman_nmb = "There is no zero nor negative numbers in Roman numerals"
        else:
            if passed_number<10:
                final_roman_nmb = self.number_to_roman(passed_number)
                

            elif passed_number>9 and passed_number<100:
                for i in str(passed_number):
                    list_of_digits.append(int(i))
                number_1 = str(list_of_digits[0]).ljust(2, '0')
                number_2 = list_of_digits[1]
                
                list_of_partly_digits = [int(number_1), number_2]
                for lst_element in list_of_partly_digits:
                    nmb = self.number_to_roman(lst_element)
                    final_roman_nmb = final_roman_nmb + nmb  

            elif passed_number<1000 and passed_number>99:
                for i in str(passed_number):
                    list_of_digits.append(int(i))
                number_1 = str(list_of_digits[0]).ljust(3, '0')
                number_2 = str(list_of_digits[1]).ljust(2, '0')
                number_3 = str(list_of_digits[2])

                list_of_partly_digits = [int(number_1), int(number_2), int(number_3)]
                for lst_element in list_of_partly_digits:
                    converted_number = self.number_to_roman(lst_element)
                    final_roman_nmb = final_roman_nmb + converted_number  

        return final_roman_nmb